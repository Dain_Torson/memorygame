package com.dain_torson.memgame.view.buttons;

import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class RotatedButton extends Button{

    private Label label;

    public RotatedButton() {

        label = new Label();
        label.setRotate(-90);
        setGraphic(new Group(label));
    }

    public void setMessage(String text) {

        label.setText(text);
    }
}
