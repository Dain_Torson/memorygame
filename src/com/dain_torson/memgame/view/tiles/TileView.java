package com.dain_torson.memgame.view.tiles;

import com.dain_torson.memgame.controller.events.TileEvent;
import com.dain_torson.memgame.data.Tile;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Rectangle;

public class TileView extends Rectangle {

    private int size;
    private Tile source;
    private GridPane parent;
    private boolean opened = false;

    public TileView(GridPane parent, Tile source, int size) {

        this.source = source;
        this.size = size;
        this.parent = parent;
        setWidth(size);
        setHeight(size);
        getStyleClass().add("tileClosed");
        addEventHandler(MouseEvent.MOUSE_PRESSED, new TilePressedHandler(this));
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
        setWidth(size);
        setHeight(size);
    }

    public void draw(int row, int col) {

        GridPane.setRowIndex(this, row);
        GridPane.setColumnIndex(this, col);
        parent.getChildren().add(this);
    }

    public void open() {
        opened = true;
        getStyleClass().clear();
        getStyleClass().add("tile" + String.valueOf(source.getValue()));
    }

    public void close() {
        opened = false;
        getStyleClass().clear();
        getStyleClass().add("tileClosed");
    }

    public boolean isOpened() {
        return opened;
    }

    private class TilePressedHandler implements EventHandler<MouseEvent> {

        private TileView source;

        public TilePressedHandler(TileView source) {
            this.source = source;
        }

        @Override
        public void handle(MouseEvent event) {

            fireEvent(new TileEvent(TileEvent.TILE_PRESSED, source));
        }
    }

   public Tile getSource() {
       return source;
   }

    public void setSource(Tile source) {
        this.source = source;
    }
}
