package com.dain_torson.memgame.controller;

import com.dain_torson.memgame.controller.events.TileEvent;
import com.dain_torson.memgame.controller.msgbox.AboutMsgBoxController;
import com.dain_torson.memgame.controller.msgbox.InputMessageBoxController;
import com.dain_torson.memgame.controller.msgbox.OutputMsgBoxController;
import com.dain_torson.memgame.data.Player;
import com.dain_torson.memgame.data.Tile;
import com.dain_torson.memgame.view.buttons.RotatedButton;
import com.dain_torson.memgame.view.tiles.TileView;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Controller {

    private List<Tile> tiles = new ArrayList<Tile>();
    private GameStateController stateController;
    private int gameSize = 4;
    private boolean tableShowed = true;
    private static Scene scene;

    @FXML
    private GridPane grid;
    @FXML
    private TableView<Player> scoreTable;
    @FXML
    private TableColumn<Player, String> playerColumn;
    @FXML
    private TableColumn<Player, Integer> scoreColumn;
    @FXML
    private Label statusLabel;
    @FXML
    private RotatedButton showButton;
    @FXML
    private HBox rightBlock;
    @FXML
    private ScrollPane scoreTablePane;
    @FXML
    private MenuItem newGameItem;
    @FXML
    private MenuItem exitGameItem;
    @FXML
    private MenuItem aboutMenuItem;

    @FXML
    private void initialize() {
        playerColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("name"));
        scoreColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("score"));
        showButton.setMessage("Hide score");
        showButton.setMaxHeight(Double.MAX_VALUE);
        showButton.setOnAction(new ShowButtonHandler());
        newGameItem.setOnAction(new NewGameItemHandler());

        aboutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    AboutMsgBoxController.show();
                }
                catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });

        exitGameItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Platform.exit();
            }
        });
    }

    public void initGame(List<String> playerNames) {

        tiles.clear();
        stateController = new GameStateController(playerNames);
        stateController.start();
        for(int tileIdx = 0; tileIdx < (gameSize*gameSize / 2); ++tileIdx) {
            for(int index = 0; index < 2; ++index) {
                Tile tile = new Tile(tileIdx);
                TileView tileView = new TileView(grid, tile, (int)(grid.getPrefWidth() / gameSize));
                tileView.addEventHandler(TileEvent.TILE_PRESSED, new TilePressedHandler());
                tile.setView(tileView);
                tiles.add(tile);
            }
        }

        Collections.shuffle(tiles);

        for(int rowIdx = 0; rowIdx < gameSize; ++rowIdx) {
            for(int colIdx = 0; colIdx < gameSize; ++colIdx) {
                tiles.get(rowIdx * gameSize + colIdx).getView().draw(rowIdx, colIdx);
            }
        }
        scoreTable.setItems(FXCollections.observableArrayList(stateController.getPlayers()));
    }

    public static Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        Controller.scene = scene;
    }

    private boolean isGameOver() {

        for(Tile tile : tiles) {
            if(!tile.getView().isOpened()) {
                return false;
            }
        }
        return true;
    }

    private class TilePressedHandler implements EventHandler<TileEvent> {

        @Override
        public void handle(TileEvent event) {
            if(!stateController.isReadyForInput()) {
                return;
            }
            stateController.update(event.getSource());
            scoreTable.getItems().clear();
            scoreTable.setItems(FXCollections.observableArrayList(stateController.getPlayers()));
            statusLabel.setText(stateController.getCurrentPlayer().getName() + "'s turn");

            if(isGameOver()) {
                try {
                    OutputMsgBoxController.show(stateController.getBestPlayer().getName() + " won!");
                }
                catch (IOException exception) {
                    exception.printStackTrace();
                }
            }

        }
    }

    void showTable() {
        rightBlock.getChildren().clear();
        rightBlock.getChildren().addAll(scoreTablePane, showButton);
        scene.getWindow().setWidth(scene.getWindow().getWidth() + scoreTablePane.getWidth());
        tableShowed = true;
        showButton.setMessage("Hide scores");
    }

    void hideTable() {
        rightBlock.getChildren().remove(scoreTablePane);
        scene.getWindow().setWidth(scene.getWindow().getWidth() - scoreTablePane.getWidth());
        tableShowed = false;
        showButton.setMessage("Show scores");
    }

    private class ShowButtonHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            if(tableShowed) {
                hideTable();
            }
            else {
                showTable();
            }
        }
    }

    private class NewGameItemHandler implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {

            try {
                InputMessageBoxController.show("Enter number of players:");
            }
            catch (IOException exception) {
                System.out.println(exception.getMessage());
                return;
            }

            String input = InputMessageBoxController.getInput();
            int numOfPlayers = 0;
            if(input != null) {
                try {
                    numOfPlayers = Integer.valueOf(input);
                }
                catch (NumberFormatException exception) {
                    return;
                }
            }

            if(numOfPlayers > 4 || numOfPlayers  < 2) {
                numOfPlayers = 2;
            }

            List<String> names = new ArrayList<String>();
            for(int playerIdx = 0; playerIdx < numOfPlayers; ++playerIdx) {
                try {
                    InputMessageBoxController.show("Enter player " +
                            String.valueOf(playerIdx + 1) + " name");
                }
                catch (IOException exception) {
                    System.out.println(exception.getMessage());
                    return;
                }

                String name = InputMessageBoxController.getInput();
                if(name == null) {
                     name = "Player " + String.valueOf(playerIdx + 1);
                }

                names.add(name);
            }

            initGame(names);

        }
    }
}
