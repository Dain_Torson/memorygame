package com.dain_torson.memgame.controller.events;

import com.dain_torson.memgame.view.tiles.TileView;
import javafx.event.Event;
import javafx.event.EventType;

public class TileEvent extends Event{

    public TileView source;
    public static EventType<TileEvent> TILE_PRESSED = new EventType("TILE_PRESSSED");


    public TileEvent(EventType<TileEvent> eventType, TileView source) {
        super(eventType);
        this.source = source;
    }

    @Override
    public TileView getSource() {
        return source;
    }
}
