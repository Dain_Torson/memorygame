package com.dain_torson.memgame.controller;

import com.dain_torson.memgame.data.Player;
import com.dain_torson.memgame.view.tiles.TileView;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;


import java.util.ArrayList;
import java.util.List;

public class GameStateController {

    private List<Player> players = new ArrayList<Player>();
    private Player currentPlayer;
    private TileView previousTile = null;
    private boolean readyForInput = true;
    private List<String> names;

    public GameStateController(List<String> playerNames) {

        this.names = playerNames;
        for(String name : names) {
            players.add(new Player(name));
        }

    }

    public void start() {

        currentPlayer = players.get(0);
        currentPlayer.setState(Player.PlayerState.ACTIVE);
    }

    public void update(TileView tileView) {

        if(!tileView.isOpened()) {
            if (currentPlayer.getState() == Player.PlayerState.ACTIVE) {
                tileView.open();
                previousTile = tileView;
                currentPlayer.setState(Player.PlayerState.PLAYING);
            } else if (currentPlayer.getState() == Player.PlayerState.PLAYING) {
                tileView.open();
                if(tileView.getSource().getValue() == previousTile.getSource().getValue()) {
                    currentPlayer.setScore(currentPlayer.getScore() + 1);
                    currentPlayer.setState(Player.PlayerState.ACTIVE);
                } else {
                    currentPlayer.setState(Player.PlayerState.INACTIVE);
                    int playerIdx = players.indexOf(currentPlayer);
                    if(playerIdx < players.size() - 1) {
                        currentPlayer = players.get(playerIdx + 1);
                    } else {
                        currentPlayer = players.get(0);
                    }
                    currentPlayer.setState(Player.PlayerState.ACTIVE);

                    Timeline timeline = new Timeline();
                    readyForInput = false;
                    timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1), new CloseTilesEventHandler(tileView)));
                    timeline.play();
                }
            }
        }
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean isReadyForInput() {
        return readyForInput;
    }

    private class CloseTilesEventHandler implements EventHandler<ActionEvent> {

        private TileView tileView;

        public CloseTilesEventHandler(TileView tileView) {

            this.tileView = tileView;
        }

        @Override
        public void handle(ActionEvent event) {
            tileView.close();
            previousTile.close();
            readyForInput = true;
        }
    }

    public Player getBestPlayer() {

        Player bestPlayer = players.get(0);

        for(Player player : players) {
            if(bestPlayer.getScore() < player.getScore()) {
                bestPlayer = player;
            }
        }

        return bestPlayer;
    }
}
