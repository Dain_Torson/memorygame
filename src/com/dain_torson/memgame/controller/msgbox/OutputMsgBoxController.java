package com.dain_torson.memgame.controller.msgbox;

import com.dain_torson.memgame.MemoryGame;
import com.dain_torson.memgame.controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class OutputMsgBoxController implements Initializable{

    private static Stage stage = null;
    private static String source = null;

    @FXML
    private Label messageLabel;
    @FXML
    private Button okButton;

    public static synchronized void show(String message) throws IOException {
        source = message;

        Parent root = FXMLLoader.load(MemoryGame.class.getResource("view/msgbox/outputMsgBoxView.fxml"));
        Scene scene = new Scene(root, 200, 150);
        stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Message");
        stage.initModality(Modality.NONE);
        stage.initOwner(Controller.getScene().getWindow());
        stage.showAndWait();
        stage.toFront();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if(source != null) {
            messageLabel.setText(source);
        }
        else {
            messageLabel.setText("Error");
        }

        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }
        });
    }


}
