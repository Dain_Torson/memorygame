package com.dain_torson.memgame.controller.msgbox;

import com.dain_torson.memgame.controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class InputMessageBoxController implements Initializable{

    private static Stage stage = null;
    private static String source = null;
    private static String input = null;

    @FXML
    private Label messageLabel;
    @FXML
    private TextField inputField;
    @FXML
    private Button okButton;
    @FXML
    private Button cancelButton;

    public static synchronized void show(String message) throws IOException {
        input = null;
        source = message;

        Parent root = FXMLLoader.load(InputMessageBoxController.class.getResource("../../view/msgbox/inputMsgBoxView.fxml"));
        Scene scene = new Scene(root, 300, 200);
        stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Message");
        stage.initModality(Modality.NONE);
        stage.initOwner(Controller.getScene().getWindow());
        stage.showAndWait();
        stage.toFront();
    }

    public static String getInput() {
        return input;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if(source != null) {
            messageLabel.setText(source);
        }
        else {
            messageLabel.setText("Error");
        }

        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                input = inputField.getText();
                if(input == "") {
                    input = null;
                }
                inputField.clear();
                stage.close();
            }
        });

        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                inputField.clear();
                stage.close();
            }
        });
    }
}
