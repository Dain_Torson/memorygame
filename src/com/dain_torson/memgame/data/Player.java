package com.dain_torson.memgame.data;


public class Player {

    private int score = 0;
    private PlayerState state = PlayerState.INACTIVE;
    private String name;

    public Player(String name) {

        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public PlayerState getState() {
        return state;
    }

    public void setState(PlayerState state) {
        this.state = state;
    }

    public enum PlayerState {INACTIVE, ACTIVE, PLAYING}

}

