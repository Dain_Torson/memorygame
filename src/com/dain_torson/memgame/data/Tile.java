package com.dain_torson.memgame.data;


import com.dain_torson.memgame.view.tiles.TileView;

public class Tile {

    private int value;
    private TileView view;

    public Tile(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public TileView getView() {
        return view;
    }

    public void setView(TileView view) {
        this.view = view;
    }
}
